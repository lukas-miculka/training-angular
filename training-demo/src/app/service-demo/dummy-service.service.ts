import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class DummyServiceService {
  dummyData: User[] = [
    { _id: 1, name: 'John', description: 'administrator', rating: 'good', type: 'A' },
    { _id: 2, name: 'Josh', description: 'groupMember', rating: 'good', type: 'A' },
    { _id: 3, name: 'Paul', description: 'groupMember', rating: 'bad', type: 'B' },
    { _id: 4, name: 'Mary', description: 'guest', rating: 'bad', type: 'C' },
  ]

  constructor() { }

  public getUsers(): Observable<User[]> {
    console.log('getUsers()');
    return Observable.of(this.dummyData);
  }

  public getUser(id): Observable<User> {
    return Observable.of(this.dummyData.find(user => user._id == id));
  }

  public addUser(name, description, rating, type): Observable<User> {
    const newId = this.dummyData[this.dummyData.length - 1]._id + 1;
    this.dummyData.push({
      _id: newId,
      name: name,
      description: description,
      rating: rating,
      type: type
    });

    return Observable.of(this.dummyData[this.dummyData.length - 1]);
  }

  public deleteUser(id) {
    const newId = this.dummyData[this.dummyData.length - 1]._id + 1;
    this.dummyData = [...this.dummyData.filter(user => user._id != id)];
  }
}

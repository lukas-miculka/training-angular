interface User {
    _id?: number,
    name: string,
    description: string,
    rating: string,
    type: string,
  }
  
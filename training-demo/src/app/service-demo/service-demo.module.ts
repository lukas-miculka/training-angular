import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ServiceDemoComponent } from './service-demo.component';
import { DummyServiceService } from './dummy-service.service';
import { DataServiceService } from './data-service.service';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    
    ButtonModule,
    InputTextModule,
    DropdownModule
  ],
  declarations: [ServiceDemoComponent],
  providers: [DummyServiceService, DataServiceService]
})
export class ServiceDemoModule { }

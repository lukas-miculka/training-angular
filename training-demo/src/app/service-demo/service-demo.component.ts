import { Component, OnInit } from '@angular/core';
import { DummyServiceService } from './dummy-service.service';
import { DataServiceService } from './data-service.service';

@Component({
  selector: 'app-service-demo',
  templateUrl: './service-demo.component.html',
  styleUrls: ['./service-demo.component.scss']
})
export class ServiceDemoComponent implements OnInit {
  newUser: User = { _id: null, name: '', description: '', rating: '', type: '' };

  dummyData: User[] = [];
  dbData: any[] = [];

  constructor(public dummyService: DummyServiceService, public dataService: DataServiceService) { }

  ngOnInit() {
    /* this.dataService.getUsers().subscribe(
      (data) => {
        this.dbData = data.docs;
        console.log('Data arrived ' + JSON.stringify(data));
      }
    ); */
    this.getUsers();
  }

  getUsers() {
    this.dummyService.getUsers().subscribe( 
      // this.processData
      (data) => {
        console.log('Dummy data', data);
        this.dummyData = data 
      }
    );
  }

  processData(data) {
    this.dummyData = data;
  }

  deleteUser(userId) {
    this.dummyService.deleteUser(userId);
  }

  deleteDbUser(userId) {
    this.dataService.deleteUser(userId);
  }

  addUser(user: User) {
    this.dummyService.addUser(user.name, user.description, user.rating, user.type);
    /* this.dataService.addUser(user.name, user.description, user.rating, user.type).subscribe(
      ok => console.log('OK', ok),
      error => console.log('ERROR', error)
    ); */
  }

  saveUser(user) {
    const index = this.dummyData.findIndex(item => item._id === user._id);
    this.dummyData[index] = { ...user, isEditMode: false } ;

    const a: any[] = [ 1, 2];
    const b: any[] = [ 3, 4];
    const c = [...a, ...b];
  }
}

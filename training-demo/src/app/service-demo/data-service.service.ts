import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class DataServiceService {
  private baseUrl = 'http://192.168.10.25:3000/user'

  constructor(private http: HttpClient) { }


  public getUsers(): Observable<any> {
    return this.http.post(this.baseUrl + '/list', {});
  }

  public getUser(id): Observable<any> {
    return this.http.get(this.baseUrl + '/' + id);
  }

  public addUser(name, description, rating, type): Observable<any> {
    const data = {
      name: name,
      description: description,
      rating: rating,
      type: type
    }
    console.log('data ' + JSON.stringify(data));
    return this.http.post(this.baseUrl + '', { date: data })
  }

  public deleteUser(id) {
    return this.http.delete(this.baseUrl + '/' + id);
  }
}

import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ButtonModule } from 'primeng/button';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComponentDemoModule } from './component-demo/component-demo.module';
import { ServiceDemoModule } from './service-demo/service-demo.module';
import { MenubarModule } from 'primeng/menubar';
import { FormsDemoModule } from './forms-demo/forms-demo.module';
import { PlaygroundModule } from './playground/playground.module';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    ComponentDemoModule,
    ServiceDemoModule,
    FormsDemoModule,
    PlaygroundModule,

    // PrimeNG
    ButtonModule,
    MenubarModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, AbstractControl } from '@angular/forms';

export class ContactRequest {
  personalData: PersonalData;
  requestType: any = '';
  text: string = '';
}

export class PersonalData {
  email: string = '';
  mobile: string = '';
  country: string = '';
}

@Component({
  selector: 'app-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss']
})
export class ReactiveFormComponent implements OnInit {
  contactForm: FormGroup;

  countries = ['USA', 'Germany', 'Italy', 'France'];

  requestTypes = ['Claim', 'Feedback', 'Help Request'];

  constructor(private fb: FormBuilder) {
    this.contactForm = new FormGroup({
      personalData: new FormGroup({
        email: new FormControl(),
        mobile: new FormControl(),
        country: new FormControl()
      }),
      requestType: new FormControl('', [Validators.required]),
      text: new FormControl('', this.validateName)
    });
  }

  ngOnInit() {
  }

  onSubmit() {

    const result: ContactRequest = Object.assign({}, this.contactForm.value);
    result.personalData = Object.assign({}, result.personalData);

    console.log('New contact:' + JSON.stringify(result));
  }

  revert() {
    // this.contactForm.reset();
    this.contactForm.reset({ personalData: new PersonalData(), requestType: '', text: '' });
  }

  private validateName(control: AbstractControl) {
    if (
      (control.value.toString().toLowerCase() !== 'jozko') &&
      (control.value.toString().toLowerCase() !== 'ferko')
    ) {
      return { validName: true };
    }
    return null;
  }

}

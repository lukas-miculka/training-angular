import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsDemoComponent } from './forms-demo.component';
import { TemplateDrivenFormComponent } from './template-driven-form/template-driven-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ReactiveFormComponent } from './reactive-form/reactive-form.component';

import { TabViewModule } from 'primeng/tabview';
import { SpecialNameDirective } from './template-driven-form/special-name.directive';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    // PrimeNG
    TabViewModule
  ],
  declarations: [
    FormsDemoComponent,
    TemplateDrivenFormComponent,
    ReactiveFormComponent,
    SpecialNameDirective,
  ]
})
export class FormsDemoModule { }

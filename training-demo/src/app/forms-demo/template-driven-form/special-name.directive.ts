import { Directive } from '@angular/core';
import { Validator, ValidatorFn, FormControl, NG_VALIDATORS, AbstractControl } from '@angular/forms';


// validation function
function validateSpecialNameFactory(): ValidatorFn {
  return (c: AbstractControl) => {
    let isValid = (c.value == 'Jozko' || c.value == 'Ferko');
    if (isValid) {
      return null;
    } else {
      return {
        specialName: {
          valid: false
        }
      };
    }
  }
}

@Directive({
  selector: '[appSpecialName][ngModel]',
  providers: [
    { provide: NG_VALIDATORS, useExisting: SpecialNameDirective, multi: true }
  ]
})
export class SpecialNameDirective implements Validator {
  validator: ValidatorFn;

  constructor() {
    this.validator = validateSpecialNameFactory();
  }

  validate(c: FormControl) {
    return this.validator(c);
  }

}


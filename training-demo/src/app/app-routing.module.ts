import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ComponentDemoComponent } from './component-demo/component-demo.component';
import { ServiceDemoComponent } from './service-demo/service-demo.component';
import { FormsDemoComponent } from './forms-demo/forms-demo.component';
import { PlaygroundComponent } from './playground/playground.component';

const routes: Routes = [
  {
    path: 'services',
    component: ServiceDemoComponent,
  },
  {
    path: 'components',
    component: ComponentDemoComponent,
  },
  {
    path: 'forms',
    component: FormsDemoComponent,
    // loadChildren: 'app/forms-demo/forms-demo.module#FormsDemoModule',
  },
  {
    path: 'playground',
    component: PlaygroundComponent,
    // loadChildren: 'app/playground/playground.module#PlaygroundModule',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

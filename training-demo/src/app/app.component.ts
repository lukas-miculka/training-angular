import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  training = { city: 'Košice', date: new Date() };

  items: MenuItem[];

  ngOnInit() {
    this.items = [
          {
            label: 'Components',
            routerLink: '/components'
          },
          {
            label: 'Forms',
            routerLink: '/forms'
          },
          {
            label: 'Services',
            routerLink: '/services'
      },
      {
        label: 'Playground',
        icon: 'fa-edit',
        routerLink:  '/playground'
      }
    ];
  }
}

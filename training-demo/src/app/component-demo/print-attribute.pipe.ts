import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'printAttribute'
})
export class PrintAttributePipe implements PipeTransform {

  transform(value: any, arg1?: any): any {
    return value[arg1];
  }

}

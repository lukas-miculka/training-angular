import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filterByAtribute'
})
export class FilterByAtributePipe implements PipeTransform {

  transform(values: any[], attribute: any, attributeValue: any): any {
    if(values == null) return null;
    return values.filter(item => item[attribute] == attributeValue);
  }

}

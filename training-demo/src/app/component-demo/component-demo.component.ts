import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-component-demo',
  templateUrl: './component-demo.component.html',
  styleUrls: ['./component-demo.component.scss']
})
export class ComponentDemoComponent implements OnInit {
  public address: any;

  public sampleAddresses: any[] = [
    { city: 'Bratislava', country: 'Slovensko' },
    { city: 'Košice', country: 'Slovensko' },
    { city: 'Banská Bystrica', country: 'Slovensko' },
    { city: 'Praha', country: 'Česko' },
    { city: 'Brno', country: 'Česko' },
    { city: 'Budapešť', country: 'Maďarsko' },
    { city: 'Vídeň', country: 'Rakousko' },

  ]

  constructor() { }

  ngOnInit() {
  }

  setAddress(event) {
    this.address = event;
  }
}

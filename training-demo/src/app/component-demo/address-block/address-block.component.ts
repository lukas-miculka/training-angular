import { Component, OnInit, Output, EventEmitter, Input  } from '@angular/core';

@Component({
  selector: 'app-address-block',
  templateUrl: './address-block.component.html',
  styleUrls: ['./address-block.component.scss']
})
export class AddressBlockComponent implements OnInit {
  @Output() onSubmitAddress = new EventEmitter();

  public countries: any[] = [  
    { label: 'Slovensko', value: 'SK' },
    { label: 'Česko', value: 'CZ' },
    { label: 'Maďarsko', value: 'HU' },
    { label: 'Rakousko', value: 'AT' }
  ]

  @Input() public address: any = {};


  constructor() { }

  ngOnInit() {
  }

  submitValues() {
    this.onSubmitAddress.emit({... this.address});
  }

}

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'decimalFormat'
})
export class DecimalFormatPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    let decimalPlaces = 0;
    if (args != null) {
      decimalPlaces = args;
    }

    return value.toLocaleString(undefined, { maximumFractionDigits: decimalPlaces });
  }

}

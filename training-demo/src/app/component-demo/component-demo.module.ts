import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ComponentDemoComponent } from './component-demo.component';
import { AddressBlockComponent } from './address-block/address-block.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {ButtonModule} from 'primeng/button';
import {InputTextModule} from 'primeng/inputtext';
import {DropdownModule} from 'primeng/dropdown';
import { FormsModule } from '@angular/forms';
import { HighlightDirective } from './highlight.directive';
import { DecimalFormatPipe } from './decimal-format.pipe';
import { FilterByAtributePipe } from './filter-by-atribute.pipe';
import { PrintAttributePipe } from './print-attribute.pipe';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    InputTextModule,
    DropdownModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  declarations: [
    ComponentDemoComponent, 
    AddressBlockComponent, 
    HighlightDirective, DecimalFormatPipe, FilterByAtributePipe, PrintAttributePipe
  ]
})
export class ComponentDemoModule { }
